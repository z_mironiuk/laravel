<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Author;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    public function editBook()
    {
        $books = Book::all();
        return view('/editbook',['books'=>$books]);
    }


    public function index()
    {
        $books = DB::table('books')->join('authors','books.author_id','=','authors.id')->paginate(10);
        return view('getbooks', ['books' => $books]);
    }

    public function showBookEdit()
    {
        $id = request('book');
        $book = Book::findOrFail($id);
        $authors = Author::all();
        return view('/editingbook', ['book' => $book, 'authors'=>$authors]);
    }

    public function edit(Request $request)
    {
        $request->validate([
            'author' => 'required',
            'title' => 'required|max:100',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $book = Book::findOrFail($id);
        $book->title = $input['title'];
        $book->author_id = $input['author'];
        $book->save();
        return redirect('/');
    }
    public function store(Request $request)
    {
        $request->validate([
            'author' => 'required',
            'title' => 'required|max:100',
        ]);
        $input = $request->all();
        $book = new Book();
        $book->author_id = $input['author'];
        $book->title = $input['title'];
        $book->save();
        return redirect('/');
    }
}
