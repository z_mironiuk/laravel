<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;

class AuthorController extends Controller
{
    public function addBook()
    {
        $Authors = Author::all();
        return view('newbook', ['authors' => $Authors]);
    }

    public function index()
    {
        $Authors = Author::all();
        return view('editauthor', ['authors' => $Authors]);
    }


    public function showAuthorEdit()
    {
        $id = request('author');
        $author = Author::findOrFail($id);
        return view('editingauthor', ['author' => $author]);
    }
    public function edit(Request $request)
    {
        $request ->validate([
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $author = Author::findOrFail($id);
        $author->Name = $input['name'];
        $author->Surname = $input['surname'];
        $author->save();
        return redirect('/');
    }
    public function store(Request $request)
    {
        $request ->validate([
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
        ]);
        $input = $request->all();
        $author = new Author();
        $author->Name = $input['name'];
        $author->Surname = $input['surname'];
        $author->save();
        return redirect('/');
    }
}
