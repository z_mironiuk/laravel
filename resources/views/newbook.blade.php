@extends('layouts.layout')

@section('content')
    <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
        @if (Route::has('login'))
            <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                @auth
                    <a href="{{ url('/home') }}" class="text-sm text-gray-700 underline">Home</a>
                @else
                    <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                    @endif
                @endif
            </div>
        @endif
        <h3>Dodawanie nowej książki:</h3>
        <form action="/newbook" method="POST">
            @csrf
            <p>Tytuł:</p>
            <input type="text" name="title" id="title" required maxlength="100">
            <p>Autor</p>
            <select name="author" id="author">
                @foreach($authors as $author)
                    <option value={{$author->id}}>{{$author->Name}} {{$author->Surname}}</option>
                @endforeach
            </select>
            <p></p>
            <input type="submit" value="Dodaj nową książkę">
        </form>
@endsection
