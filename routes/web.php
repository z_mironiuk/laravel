<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/newauthor', function () {
    return view('newauthor');
});

Route::get('/newbook', [\App\Http\Controllers\AuthorController::class, 'addBook']);

Route::get('/editauthor',[\App\Http\Controllers\AuthorController::class, 'index']);



Route::get('/editbook', [\App\Http\Controllers\BookController::class, 'editBook']);


Route::post('/editingbook', [\App\Http\Controllers\BookController::class,'showBookEdit']);

Route::post('/editingauthor',[\App\Http\Controllers\AuthorController::class, 'showAuthorEdit']);

Route::post('/modifyAuthor', [\App\Http\Controllers\AuthorController::class, 'edit']);

Route::post('/modifyBook', [\App\Http\Controllers\BookController::class, 'edit']);


Route::Post('/newauthor',[\App\Http\Controllers\AuthorController::class, 'store']);

Route::Post('/newbook', [\App\Http\Controllers\BookController::class,'store']);

Route::get('/getbooks', [\App\Http\Controllers\BookController::class, 'index']);
